// массив записей
var data_todo = (localStorage.getItem('todoList'))
  ? JSON.parse(localStorage.getItem('todoList'))
  : [];

var data_done = (localStorage.getItem('doneList'))
  ? JSON.parse(localStorage.getItem('doneList'))
  : [];

// синхронизация localStorage и Массива JS
function storageSync() {
  localStorage.setItem('todoList', JSON.stringify(data_todo));
  localStorage.setItem('doneList', JSON.stringify(data_done));
}

// рисуем начальный список
renderTodoList();
renderDoneList();


// Добавить новый элемент в список
document.getElementById('btn-create').addEventListener('click', function () {
  document.getElementById("input-search").value = '';
  document.getElementById('completed-tasks-list').innerHTML = '';
  renderDoneList();

  var value = document.getElementById('input-create').value;
  if (value) {
    addItemToDOM(value);
    document.getElementById('input-create').value = '';

    data_todo.push(value);  
    storageSync();
  } 
});


document.getElementById('input-search').addEventListener('keyup', function() {
  var value = document.getElementById('input-search').value;
  removeItems();
  if(value) {
    value = value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    var regexp = new RegExp(value, 'g');
    for(var i = 0; i < data_done.length; i++) {
      if(data_done[i].match(regexp)) {
        addItemToList(data_done[i]);
      }
    }
    var list = document.getElementById('completed-tasks-list');
    if(list.firstChild == null) {
      document.getElementById('completed-tasks-list').innerHTML = 'Совпадений не найдено';
    }
  } else {
    document.getElementById('completed-tasks-list').innerHTML = '';
    renderDoneList();
  }
});

function removeItems() {
  var items = document.getElementsByClassName('done-item');
  var list = document.getElementById('completed-tasks-list');
  while(items.length > 0) {
    list.removeChild(items[0]);
  }
}

function renderTodoList() {
  if (!data_todo.length) return;

  for (var i = 0; i < data_todo.length; i++) {
    var value = data_todo[i];
    addItemToDOM(value);
  }
}

function renderDoneList() {
  if(!data_done.length) return;

  for(var i = 0; i < data_done.length; i++) {
    addItemToList(data_done[i]);
  }
}

function addItemToDOM(text) { 
  var list = document.getElementById('todo-list');

  var item = document.createElement('li'); 
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
      <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
    </span>
  </div>`;
  item.innerHTML = listItemView;

  // добавим слушатель для удаления
  var buttonDelete = item.getElementsByClassName('item-btn-del')[0];
  buttonDelete.addEventListener('click', removeItem);

  list.appendChild(item);
}

function removeItem(e) {
  document.getElementById("input-search").value = '';
  document.getElementById('completed-tasks-list').innerHTML = '';
  renderDoneList();

  var item = this.parentNode.parentNode.parentNode;
  var list = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;

  data_todo.splice(data_todo.indexOf(value), 1);
  storageSync();

  list.removeChild(item);

  data_done.push(value);
  storageSync();

  addItemToList(value);
}

function addItemToList(text) {
  var task_list = document.getElementById("completed-tasks-list");
  var item = document.createElement('li');
  item.classList = 'collection-item done-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
  </div>`;

  item.innerHTML = listItemView;
  task_list.appendChild(item);
}
